package com.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class KubeTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(KubeTestApplication.class, args);

        //Thread with lambda exp
        //        RunnableTest runnableTest = new RunnableTest();
        //        runnableTest.test();
        //        t.test();
    }

}
