package com.test.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class StatusController {

    @GetMapping("/ping")
    public String ping() {
        log.info("Test Service is running....");
        return "Test Service is running...";
    }
}
